## App to search abbreviation

- Creator: Benjamin Bolling (benjamin.bolling@ess.eu)
- Created: 2020-10-20
- Last update (proper restructuring): 2023-01-11

### Purpose
Input abbreviation, return if key exists.

### Description
A simple CLI-based Python script that can be used to search for abbreviations used at ESS.

### Tutorial
How to use the script.

#### CLI help
Input: --help -h

```python
python ESSdict.py --help
```

Output:
- This help message.

#### CLI search for abbreviation
Input: key (in string format)

Example: Searching for the abbreviation 'bpm'.

```python
python ESSdict.py bpm
```

Output:
- If found: Resulting values of the abbreviation
- If not found: A message stating that the abbreviation is not defined in the dictionary file.
- Search is case insensitive; but shows correct lower/upper cases in parenthesis.

#### Print all
Input: --print_all

```python
python ESSdict.py --print_all
```

Output:
- Full dictionary printed, one keyword per line.

#### GUI
Input: --tk

```python
python ESSdict.py --tk
```

Output:
- The acronyms finder with a Python-native graphical user interface.