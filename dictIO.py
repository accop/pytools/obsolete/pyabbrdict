"""
App to search abbreviation. Input abbreviation, return if key exists.

Creator: Benjamin Bolling (benjamin.bolling@ess.eu)
Created: 2020-10-20
Last update (using CSV files and simplified): 2023-03-28
"""

import csv

def getCSV(f):
    return {line[0]: line[1] for line in csv.reader(f, delimiter=';')}

def structureResults(k,results):
    obj = ['Values found for key {}:\n'.format(k)]
    for result in results:
        obj.append('{} ({})'.format(result[1],result[0]))
    return obj

def showResults(obj):
    for line in obj:
        print(line)
    print('\n')

def keySearch(abbrDict, key):
    results = []
    allKeysUpper = [k.upper() for k in abbrDict.keys()]
    if key.upper() in allKeysUpper:
        keys_sensitive = [k for k in abbrDict.keys() if key.upper() == k.upper()]
        for k in keys_sensitive:
            results.append([k, abbrDict[k]])
        return True, results
    else:
        results.append("Key could not be found, sorry for that.")
        results.append(" ")
        results.append("Feel free to email benjamin.bolling@ess.eu with what key is missing.")
        return False, results
