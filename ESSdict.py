"""
App to search abbreviation. Input abbreviation, return if key exists.

Creator: Benjamin Bolling (benjamin.bolling@ess.eu)
Created: 2020-10-20
Last update (using CSV files and simplified): 2023-03-28
"""
import sys
from dictIO import getCSV, showResults, keySearch, structureResults
import UI
import os

fn = os.path.join(os.path.dirname(os.path.realpath(__file__)),"acronyms.csv")
with open(fn, "r") as f:
    abbrDict = getCSV(f)

if len(sys.argv) > 1:
    if sys.argv[1] == '--help' or sys.argv[1] == '-h':
        print('\n--------------------------------------------------------\nA simple script searching for abbreviations used at ESS.\n--------------------------------------------------------\n\nInput options: \n  --help -h : This help message.\n  --print_all : Prints all keys with all their value(s).\n  --tk : Use UI with tkinter.\n  str KEY keyword: The key to search for in the dictionary.\n\n')
    elif sys.argv[1] == '--print_all':
        print('\nPrinting all keys and values defined:\n---------------------------\n')
        for k, v in abbrDict.items():
            print('{}  ==  {}'.format(k, v))
        print('\n---------------------------\n')
    elif sys.argv[1] == '--tk':
        UI.run(abbrDict)
    else:
        found, results = keySearch(abbrDict, sys.argv[1])
        if found:
            showResults(structureResults(sys.argv[1], results))
else:
    if sys.version_info.major == 3:  # Python3
        keysearch = input("Type the abbreviation: ")
        found, results = keySearch(abbrDict, keysearch)
        if found:
            showResults(structureResults(keysearch, results))
        else:
            showResults(results)
    elif sys.version_info.major == 2:  # Python2
        keysearch = str(raw_input("Type the abbreviation: "))
        found, results = keySearch(abbrDict, keysearch)
        if found:
            showResults(structureResults(keysearch, results))
        else:
            showResults(results)
