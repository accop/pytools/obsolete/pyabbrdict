import tkinter as tk
from dictIO import structureResults, keySearch

def run(abbrDict):
    # Top level window
    frame = tk.Tk()
    frame.title("Acronyms Finder")
    frame.geometry('300x200')

    def findKeys(abbrDict):
        ks = inp.get()
        found, results = keySearch(abbrDict, ks)
        if found:
            text = '\n'.join(structureResults(ks, results))
        else:
            # text = 'Key {} not found.'.format(ks)
            text = '\n'.join(results)
        lbl.config(state='normal')
        lbl.delete('1.0', tk.END)
        lbl.insert(1.0, text)
        lbl.config(state='disabled')

    inp = tk.Entry(frame, width=40, wrap=None)
    inp.pack()
    pb = tk.Button(frame, width=40, text="Print", command = lambda: findKeys(abbrDict))
    pb.pack()
    lbl = tk.Text(frame, width=40, height=20)
    lbl.pack()
    lbl.config(state='disabled')
    frame.mainloop()